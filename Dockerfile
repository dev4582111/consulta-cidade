# Use a imagem base Python
FROM python:3.8-slim

# Define o diretório de trabalho dentro do contêiner
WORKDIR /app

# Copia os arquivos necessários para dentro do contêiner
COPY script.py .
COPY requirements.txt .

# Instala as dependências
RUN pip install Flask --no-cache-dir -r requirements.txt

# Define o comando padrão a ser executado quando o contêiner for iniciado
CMD ["python", "script.py"]
