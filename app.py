from flask import Flask, render_template, request
from script import obter_cidade_por_cep

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/consulta', methods=['POST'])
def consulta():
    cep = request.form['cep']
    cidade = obter_cidade_por_cep(cep)
    return render_template('resultado.html', cidade=cidade)

if __name__ == '__main__':
    app.run(debug=True)
