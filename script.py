import requests

def obter_cidade_por_cep(cep):
    url = f'http://viacep.com.br/ws/{cep}/json/'
    try:
        response = requests.get(url)
        data = response.json()
        if 'erro' not in data:
            return data['localidade']
        else:
            return "CEP não encontrado"
    except requests.exceptions.RequestException as e:
        return f"Erro ao obter dados: {e}"

def main():
    cep = input("Digite o seu CEP: ")
    cidade = obter_cidade_por_cep(cep)
    print(f"A cidade correspondente ao CEP {cep} é: {cidade}")

if __name__ == "__main__":
    main()
